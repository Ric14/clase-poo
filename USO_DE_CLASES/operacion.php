<?php

class Operacion{
    
    public $OperacionRealizar = "";

    public function Cuadrado($lado){
        return ($lado * 4);
    
    }

    public function TrianguloEquilatero($lado){
        return ($lado * 3);
    
    }

    public function Rombo($lado){
        return ($lado * 4);
    
    }

    public function Pentagono($lado){
        return ($lado * 5);
    
    }
    public function Hexagono($lado){
        return ($lado * 6);
    
    }

    public function ResultadoOperacion($lado){

        switch ($this->OperacionRealizar) { 
            case 'cuadrado':
                return $this->Cuadrado($lado);
                break;

            case 'triangulo':
                return $this->TrianguloEquilatero($lado);
                break;

            case 'rombo':
                return $this->Rombo($lado);
                break;
    
            case 'pentagono':
                return $this->Pentagono($lado);
                break;

            case 'hexagono':
                return $this->Hexagono($lado);
                break;
           
            default:
                return 'Operacion a realizar no definida';
                break;
        }

    }

}


?>
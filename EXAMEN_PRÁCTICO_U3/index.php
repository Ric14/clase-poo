<?php

class Perro
{
    private $nombre;
    private $ladrar;

    private function ladrar(){
        echo "Guau!";
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function getNombre(){
        return $this->nombre;
    }

}

$cachorro = new Perro();
 
$cachorro->setNombre("Firulais");
echo "Nombre del perro: ".$cachorro->getNombre();

class Bulldog extends Perro {
    public function Accesoladrar(){
        $this->ladrar();
    }
}

?>
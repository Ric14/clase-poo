<?php

class Computadora{

    public $discoduro = 0;
    public $memoriaram = 0;
    public $SO = "";
    public $tarjetagráfica = "";
    public $procesador = "";


    public function setDiscoduro($gb_discoduro){
        $this->discoduro = $gb_discoduro;
    }

    public function getDiscoduro(){
        return $this->discoduro;
    }

    public function setMemoriaram($gb_memoriaram){
        $this->memoriaram = $gb_memoriaram;
    }

    public function getMemoriaram(){
        return $this->memoriaram;
    }

    public function setSO($Win_SO){
        $this->SO = $Win_SO;
    }

    public function getSO(){
        return $this->SO;
    }
}

$objPC = new Computadora();

$objPC->setDiscoduro(320);

    echo $objPC->getDiscoduro()."<br>";

$objPC = new Computadora();

$objPC->setMemoriaram(8);

    echo $objPC->getMemoriaram()."<br>";

$objPC = new Computadora();

$objPC->setSO(Windows10);

    echo $objPC->getSO()."<br>";

?>
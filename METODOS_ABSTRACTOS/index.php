<?php

abstract class Transporte{

    abstract public function Mantenimiento();
}

class Avion extends Transporte{
    
    public function Mantenimiento(){

        echo "Revisión de alas ";
    }

}

class Automovil extends Transporte{
    
    public function Mantenimiento(){

        echo "Revisión de motor ";
    }

}

class MetroBus extends Transporte{
    
    public function Mantenimiento(){

        echo "Rotación de llantas ";
    }

}

class Barco extends Transporte{
    
    public function Mantenimiento(){

        echo "Limpieza de velas ";
    }

}

class Bicicleta extends Transporte{
    
    public function Mantenimiento(){

        echo "Inspección del sistema de frenos ";
    }

}


$obj = new Avion();
$obj -> Mantenimiento();
$obj = new Automovil();
$obj -> Mantenimiento();
$obj = new MetroBus();
$obj -> Mantenimiento();
$obj = new Barco();
$obj -> Mantenimiento();
$obj = new Bicicleta();
$obj -> Mantenimiento();

?>